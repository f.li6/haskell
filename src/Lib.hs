module Lib where

import Data.Bool (Bool)
import Data.String (String)
import Test.QuickCheck

-- fmap :: Functor f => (a -> b) -> f a -> f b

-- Implement Functor instances for the following datatypes.
-- Use the QuickCheck properties we showed you to validate them.
newtype Identity a = Identity a deriving (Eq, Show)

-- Identity here must have the kind * -> *: Expected kind ‘* -> *’, but ‘Identity a’ has kind ‘*’
-- Identity :: * -> *
-- Identity Int :: *
instance Functor Identity where
  fmap f (Identity a) = Identity $ f a

instance Arbitrary a => Arbitrary (Identity a) where
  arbitrary = Identity <$> arbitrary

-- structure type
functorIdentity :: (Functor f, Eq (f a)) => f a -> Bool
functorIdentity f = fmap id f == f

functorCompose :: (Functor f, Eq (f c)) => (a -> b) -> (b -> c) -> f a -> Bool
functorCompose f g x = (fmap g (fmap f x)) == fmap (g . f) x

functorCompose' :: (Functor f, Eq (f c)) => Fun a b -> Fun b c -> f a -> Bool
functorCompose' (Fn f) (Fn g) x = (fmap g (fmap f x)) == fmap (g . f) x

-- real type
f1611 :: Identity String -> Bool
f1611 = functorIdentity

f1612 :: Identity Int -> Bool
f1612 = functorCompose (+ 1) (* 2)

f1613 :: Fun Int Int -> Fun Int Int -> Identity Int -> Bool
f1613 = functorCompose'

-- --*******************************************-- --
data Pair a = Pair a a deriving (Eq, Show)

instance Functor Pair where
  fmap f (Pair x y) = Pair (f x) (f y)

instance Arbitrary a => Arbitrary (Pair a) where
  arbitrary = do
    l <- arbitrary
    Pair l <$> arbitrary

-- real type
f1621 :: Pair String -> Bool
f1621 = functorIdentity

f1622 :: Pair Int -> Bool
f1622 = functorCompose (+ 1) (* 2)

f1623 :: Fun Int Int -> Fun Int Int -> Pair Int -> Bool
f1623 = functorCompose'

-- --*******************************************-- --

data Two a b = Two a b deriving (Eq, Show)

instance Functor (Two a) where
  fmap f (Two a b) = Two a (f b)

instance (Arbitrary a, Arbitrary b) => Arbitrary (Two a b) where
  arbitrary = do
    a <- arbitrary
    Two a <$> arbitrary

-- real type
f1631 :: Two String Float -> Bool
f1631 = functorIdentity

f1632 :: Two String Float -> Bool
f1632 = functorCompose (+ 1) (* 2)

f1633 :: Fun Float Int -> Fun Int Int -> Two String Float -> Bool
f1633 = functorCompose'

-- --*******************************************-- --

data Three a b c = Three a b c deriving (Eq, Show)

instance Functor (Three a b) where
  fmap f (Three a b c) = Three a b (f c)

instance (Arbitrary a, Arbitrary b, Arbitrary c) => Arbitrary (Three a b c) where
  arbitrary = do
    a <- arbitrary
    b <- arbitrary
    Three a b <$> arbitrary

-- real type
f1641 :: Three String Char Int -> Bool
f1641 = functorIdentity

f1642 :: Three String Char Int -> Bool
f1642 = functorCompose (+ 1) (* 2)

f1643 :: Fun Int Int -> Fun Int Int -> Three String Char Int -> Bool
f1643 = functorCompose'

-- --*******************************************-- --
-- Type Constructor types = Data Constructor parameters types
data Three' a b = Three' a b b deriving (Eq, Show)

-- instance Functor (Type Constructor *->*) where
-- fmap f (Data Constructor parameters placeholder) = Data Constructor parameters placeholder with f
instance Functor (Three' a) where
  fmap f (Three' a b c) = Three' a (f b) (f c)

-- Data Constructor parameters types
-- Data Constructor parameters placeholder
instance (Arbitrary a, Arbitrary b) => Arbitrary (Three' a b) where
  arbitrary = do
    a <- arbitrary
    b <- arbitrary
    Three' a b <$> arbitrary

-- real type
f1651 :: Three' String Int -> Bool
f1651 = functorIdentity

f1652 :: Three' String Int -> Bool
f1652 = functorCompose (+ 1) (* 2)

f1653 :: Fun Int Int -> Fun Int Int -> Three' String Int -> Bool
f1653 = functorCompose'

-- --*******************************************-- --
-- --*******************************************-- --

data Possibly a = LolNope | Yeppers a deriving (Eq, Show)

instance Functor Possibly where
  fmap f (Yeppers a) = Yeppers (f a)
  fmap _ LolNope = LolNope

instance (Arbitrary a) => Arbitrary (Possibly a) where
  arbitrary = do
    Yeppers <$> arbitrary

-- real type
f1751 :: Possibly String -> Bool
f1751 = functorIdentity

f1752 :: Possibly Int -> Bool
f1752 = functorCompose (+ 1) (* 2)

f1753 :: Fun Int Int -> Fun Int Int -> Possibly Int -> Bool
f1753 = functorCompose'

data Sum a b = First a | Second b deriving (Eq, Show)

instance Functor (Sum a) where
  fmap _ (First a) = First a
  fmap f (Second b) = Second (f b)
